
# haxball-2019 (name subject to change)

## Setup
Install python requirements
```bash
pip install -r requirements.txt
```

Add following line to hosts file (`c:\windows\system32\drivers\etc\hosts` on windows, `/etc/hosts` on linux)
```
127.0.0.1 2019.haxball.com
```

## Usage

Start server
```bash
python server.py
```

Only one connection from game and bot are active, new connections will close earlier.

Bot and game may connect and disconnect at any time.

Run bot example
```bash
python bot_example.py
```

Open `2019.haxball.com` in browser, game will connect automatically.
This is original game with slight modifications to sends state to bot and accepts input from bot.
